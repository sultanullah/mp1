//EECE 210 Machine Problem 1: Guitar Hero
//Guitar String Class

import java.util.Queue;
import java.util.LinkedList;
/**
 * 
 * @author Sultan Ullah(29128113)and Kevin Chan(39975131) This class 
 *         represents a real guitar string that vibrates when given a 
 *         frequency. A ring buffer implementing the Karplus-Strong
 *         algorithm is used to create the vibrations when the guitar string is plucked.  
 */
public class GuitarString {
	//this constant represents the slight dissipation of the wave created from vibration.
	private static final double ENERGY_DECAY_FACTOR = 0.996;
	
	//This field is used to keep track of the ring buffer.
	private	Queue<Double> RingBuffer = new LinkedList<Double>();

	/**
	 * Constructs a new guitar string object.
	 *
	 * @param frequency 
	 * 				The frequency of the vibrations on the guitar string.
	 * @throws IllegalArgumentException 
	 * 				If the frequency is less than 0 or
	 *              the size of the created ring buffer is less than 2.
	 */
	public GuitarString(double frequency) throws IllegalArgumentException {
		if (frequency <= 0 ) {
			throw new IllegalArgumentException();
		} else {
			double N = (StdAudio.SAMPLE_RATE/frequency);
			double roundedN = Math.round(N);
			for (int i = 0; i < roundedN; i++ ) {
				RingBuffer.add(0.0);
			}
		}
		if (RingBuffer.size() < 2) {
			throw new IllegalArgumentException(); 
		}
	}

	/**
	 * Constructs a new guitar string object.
	 *
	 * @param init
	 * 			The array that gets stored in the ring buffer. 
	 * 			This array is used to test the Guitar String class.
	 * @throws IllegalArgumentException 
	 * 			If the array has less than 2 elements.
	 */
	public GuitarString(double[] init) throws IllegalArgumentException {
		if (init.length < 2) {
			throw new IllegalArgumentException();
		} else {
			for (int i = 0; i < init.length; i++) {
				RingBuffer.add(init[i]);
			}
		}
	}

	/**
	 * Pluck the guitar string.
	 * 
	 * @return Replaces all elements in the ring buffer with a random number
	 *         from -0.5 inclusive to 0.5 exclusive, these are the bounds used
	 *         to sample displacement.
	 */
	void pluck() {
		int size = RingBuffer.size();
		for (int i = 0; i < size; i++) {
			RingBuffer.remove();
			RingBuffer.add(StdRandom.uniform(-0.5, 0.5));
		}
	}
	
	/**
	 * Tic applies the Karplus-Strong algorithm only once to the ring buffer.
	 * 
	 * @return The ring buffer with the Karplus-Strong updated once. The first value
	 *         is removed and added to the second value, the result is multiplied by 
	 *         energy decay factor, and moved to the back of the ring buffer.
	 *        
	 */
	void tic() {
		double firstValue = RingBuffer.remove();
		double secondValue = RingBuffer.peek();
		double result = ((firstValue + secondValue)/2.0)*ENERGY_DECAY_FACTOR;
		RingBuffer.add(result);
	}

	/**
	 * Returns a sample of the vibration wave.
	 *
	 * @return The first values of the ring buffer.
	 */
	double sample() {
		return RingBuffer.peek();
	}
}