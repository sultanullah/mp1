//EECE 210 Machine Problem 1: Guitar Hero
//Guitar210 Class

/**
 * 
 * @author Sultan Ullah(29128113)and Kevin Chan(39975131) This class 
 *         represents and keeps track of a real instrument 
 *         that has multiple strings.  
 */
//The Guitar210 is part of the Guitar interface.
public class Guitar210 implements Guitar {
    
    //constant containing values that can be input from the keyboard
	//that are mapped to different frequencies.
    public static final String KEYBOARD =
        "q2we4r5ty7u8i9op-[=zxdcfvgbnjmk,.;/' ";  // keyboard layout

    //This field maintains the multiple strings used for the instrument.
    private GuitarString[] allStrings = new GuitarString[37];
  
    /**
     * Constructs a new Guitar210 object.
     */
    public Guitar210(){
    	for (int i = 0; i < allStrings.length; i++) {
    		double hertz = 440.0 * Math.pow(2, (i - 24.0)/12.0);
    		allStrings[i] = new GuitarString(hertz);
    	}
    }
    
	/**
	 * Plays the note that corresponds to the pitch.
	 *
	 * @param pitch 
	 * 			The pitch which will be mapped to a 
	 *          frequency on the chromatic scale. If the
	 *          frequency is not on the scale note is not played.
	 */
    public void playNote(int pitch) {
    	try {
    		allStrings[pitch + 12].pluck();
    	    }
    	catch (ArrayIndexOutOfBoundsException exception) {}
    }   	
    			  			
	/**
	 * Checks to see if the character can be mapped to an instrument string.
	 *
	 * @param string 
	 * 		  	A character on the keyboard.
	 * @return true if the character corresponds to a string.	         	
	 */
    public boolean hasString(char string) {
    	return(KEYBOARD.contains(String.valueOf(string)));
    }
    	   	
    /**
	 * Plucks the corresponding string for the character.
	 *
	 * @param string 
	 * 		  	A character on the keyboard that maps to a
	 *          string that is plucked.
     */
    public void pluck(char string) {
        int frequencyIndex = KEYBOARD.indexOf(string);
        allStrings[frequencyIndex].pluck();
    }
    
    /**
  	 * Returns the current of the sound
  	 *
  	 * @return sum of all the strings for the instrument.
     */
    public double sample() {
    	double final_sum = 0.0;
    	for (int i = 0; i < allStrings.length; i++) {
    		final_sum = allStrings[i].sample() + final_sum;
    	}
    	return final_sum;
    }
    
    /**
  	 * makes each string tic forward, adding to the simulation
  	 * of a guitar. 
  	 */
    public void tic() {
        for(int i = 0; i < allStrings.length; i++){
        	allStrings[i].tic();
        }
    	total_tic = total_tic + 1;
    }
    
    //used to keep track of every call to tic()
    public static int total_tic = 0;
    
    /**
   	 * returns the amount of time the method tic() is called.
   	 * 
   	 * @return number of times tic() is called. 
   	 */
    public int time() {
    	return total_tic;
    }
}